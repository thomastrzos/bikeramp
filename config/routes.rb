Rails.application.routes.draw do
  namespace :api do
    resources :trips, only: [:create]
    get 'stats/weekly'
    get 'stats/monthly'
  end
end
