require 'rails_helper'

RSpec.describe Api::StatsController, type: :controller do

  describe "GET #weekly" do
    it "returns http success" do
      get :weekly
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #monthly" do
    it "returns http success" do
      get :monthly
      expect(response).to have_http_status(:success)
    end
  end

end
