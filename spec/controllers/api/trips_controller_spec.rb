require 'rails_helper'

RSpec.describe Api::TripsController, type: :controller do
  describe 'POST /trips' do
    let(:valid_attributes) do
      { start_address: 'Plac Europejski 2, Warszawa, Polska',
        destination_address: 'Plac Europejski 2, Warszawa, Polska',
        price: 25.5, date: '30-04-2018' }
    end

    context 'request with valid attributes' do
      before do
        post :create, params: valid_attributes
      end

      it 'creates trip' do
        expect(response_body['trip']['date']).to eq('2018-04-30')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'request with invalid attributes' do
      before do
        post :create, params: {}
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end
end
