require 'rails_helper'

RSpec.describe GoogleDistanceMatrix do
  describe 'call' do
    context 'different addresses' do
      start_address = 'Piękna 5, Warszawa, Polska'
      destination_address = 'Chyliczkowska 14, Piaseczno, Polska'
      let(:google_distance_matrix_result) { GoogleDistanceMatrix.new.call(start_address, destination_address) }

      it 'returns correct distance' do
        expect(google_distance_matrix_result.to_f).to eq(19.4)
      end
    end

    context 'the same address' do
      start_address = 'Plac Europejski 2, Warszawa, Polska'
      destination_address = 'Plac Europejski 2, Warszawa, Polska'
      let(:google_distance_matrix_result) { GoogleDistanceMatrix.new.call(start_address, destination_address) }

      it 'returns correct distance' do
        expect(google_distance_matrix_result.to_f).to eq(0.0)
      end
    end
  end
end
