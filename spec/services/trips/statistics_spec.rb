require 'rails_helper'

RSpec.describe Trips::Statistics do
  let!(:trip_01) { Trip.create(distance: 10.0, price: 10.0, date: '2018-04-01') }
  let!(:trip_02) { Trip.create(distance: 20.0, price: 20.0, date: '2018-04-30') }
  let!(:trip_03) { Trip.create(distance: 30.0, price: 30.0, date: '2018-04-30') }
  let!(:trip_04) { Trip.create(distance: 40.0, price: 40.0, date: '2018-04-30') }

  describe 'get_weekly_stats' do
    today = Date.new(2018,4,30)
    let(:weekly_stats) { Trips::Statistics.get_weekly_stats(today) }
    it 'returns correct total_distance' do
      expect(weekly_stats[:total_distance].to_f).to eq(90.0)
    end

    it 'returns correct total_price' do
      expect(weekly_stats[:total_price].to_f).to eq(90.0)
    end
  end

  describe 'get_monthly_stats' do
    today = Date.new(2018,4,30)
    let(:monthly_stats) { Trips::Statistics.get_monthly_stats(today) }

    context 'last day of month' do
      it 'returns correct formatted_day' do
        expect(monthly_stats.last[:day]).to eq("April, 30th")
      end

      it 'returns correct total_distance' do
        expect(monthly_stats.last[:total_distance].to_f).to eq(90.0)
      end

      it 'returns correct avg_ride' do
        expect(monthly_stats.last[:avg_ride].to_f).to eq(30.0)
      end

      it 'returns correct avg_price' do
        expect(monthly_stats.last[:avg_price].to_f).to eq(30.0)
      end
    end

    context 'first day of month' do
      it 'returns correct formatted_day' do
        expect(monthly_stats.first[:day]).to eq("April, 1st")
      end

      it 'returns correct total_distance' do
        expect(monthly_stats.first[:total_distance].to_f).to eq(10.0)
      end

      it 'returns correct avg_ride' do
        expect(monthly_stats.first[:avg_ride].to_f).to eq(10.0)
      end

      it 'returns correct avg_price' do
        expect(monthly_stats.first[:avg_price].to_f).to eq(10.0)
      end
    end
  end

  describe 'get_total_distance_from_date' do
    today = Date.new(2018,4,30)
    let(:total_distance) { Trips::Statistics.send(:get_total_distance_from_date, today ) }
    it 'returns correct value' do
      expect(total_distance.to_f).to eq(90.0)
    end
  end

  describe 'get_total_price_from_date' do
    today = Date.new(2018,4,30)
    let(:total_price) { Trips::Statistics.send(:get_total_price_from_date, today ) }
    it 'returns correct value' do
      expect(total_price.to_f).to eq(90.0)
    end
  end

  describe 'get_avg_distance_from_date' do
    today = Date.new(2018,4,30)
    let(:avg_distance) { Trips::Statistics.send(:get_avg_distance_from_date, today ) }
    it 'returns correct value' do
      expect(avg_distance.to_f).to eq(30.0)
    end
  end

  describe 'get_avg_price_from_date' do
    today = Date.new(2018,4,30)
    let(:avg_price) { Trips::Statistics.send(:get_avg_price_from_date, today ) }
    it 'returns correct value' do
      expect(avg_price.to_f).to eq(30.0)
    end
  end

end
