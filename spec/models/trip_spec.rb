require 'rails_helper'

RSpec.describe Trip, type: :model do
  it { should validate_presence_of(:distance) }
  it { should validate_presence_of(:price) }
  it { should validate_presence_of(:date) }
end
