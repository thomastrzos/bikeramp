class CreateTrips < ActiveRecord::Migration[5.2]
  def change
    create_table :trips do |t|
      t.decimal :distance
      t.decimal :price
      t.date :date, index: true

      t.timestamps
    end
  end
end
