class Trip < ApplicationRecord
  validates :distance, presence: true
  validates :price, presence: true
  validates :date, presence: true
end
