json.array! @monthly_array_stats do |day_stats|
  json.day day_stats[:day]
  json.total_distance "#{day_stats[:total_distance]}km"
  json.avg_ride "#{day_stats[:avg_ride]}km"
  json.avg_price "#{day_stats[:avg_price]}PLN"
end
