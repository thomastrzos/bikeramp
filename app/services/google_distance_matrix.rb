class GoogleDistanceMatrix
  def call(start_address, destination_address)
    start_coordinates = get_coordinates(start_address)
    destination_coordinates = get_coordinates(destination_address)

    return 0.0 if same_coordinates?(start_coordinates, destination_coordinates)

    start_coordinates_as_string = array_coordinates_to_string(start_coordinates)
    destination_coordinates_as_string = array_coordinates_to_string(destination_coordinates)
    response = HTTParty.get(request(start_coordinates_as_string, destination_coordinates_as_string))
    parse_and_get_decimal_value_from_response(response)
  end

  private

  def get_coordinates(address)
    Geocoder.coordinates(address)
  end

  def array_coordinates_to_string(coordinates)
    coordinates.join(',')
  end

  def request(start_coordinates, destination_coordinates)
    "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=#{start_coordinates}&destinations=#{destination_coordinates}&key=AIzaSyDXyE06Uzl86fo_fOEj_4axiwDbAdIk3AU"
  end

  def parse_and_get_decimal_value_from_response(response)
    distance_as_text = JSON.parse(response.to_s)['rows'][0]['elements'][0]['distance']['text']
    distance_as_text.split.first.to_d
  end

  def same_coordinates?(coordinates1, coordinates2)
    coordinates1.to_set == coordinates2.to_set
  end
end
