class Trips::Statistics
  class << self
    def get_weekly_stats(today)
      total_weekly_distance = 0
      total_weekly_price = 0
      (today.at_beginning_of_week..today.at_end_of_week).each do |date|
        total_weekly_distance += get_total_distance_from_date(date)
        total_weekly_price += get_total_price_from_date(date)
      end

      { total_distance: total_weekly_distance, total_price: total_weekly_price }
    end

    def get_monthly_stats(today)
      result_array = []
      (today.at_beginning_of_month..today.at_end_of_month).each do |date|
        formatted_date = date.strftime("%B, #{date.day.ordinalize}")
        total_distance = get_total_price_from_date(date)
        avg_ride = get_avg_distance_from_date(date)
        avg_price = get_avg_price_from_date(date)
        result_array << { day: formatted_date,
                          total_distance: total_distance, avg_ride: avg_ride,
                          avg_price: avg_price }
      end
      result_array
    end

    def get_total_distance_from_date(date)
      Trip.where(date: date).pluck(:distance).sum
    end

    def get_total_price_from_date(date)
      Trip.where(date: date).pluck(:price).sum
    end

    def get_avg_distance_from_date(date)
      Trip.where(date: date).average(:distance)
    end

    def get_avg_price_from_date(date)
      Trip.where(date: date).average(:price)
    end
  end
end
