class Api::TripsController < ApplicationController
  attr_accessor :trip

  def create
    start_address = trips_params[:start_address]
    destination_address = trips_params[:destination_address]
    if invalid_address?(start_address) || invalid_address?(destination_address)
      render(json: { error: 'You have to give correct attributes.' }, status: :unprocessable_entity) && return
    end

    price = trips_params[:price]
    date = trips_params[:date]
    distance = GoogleDistanceMatrix.new.call(start_address, destination_address)

    @trip = Trip.new(distance: distance, price: price, date: date)

    if trip.save
      render json: { trip: trip }, status: 201
    else
      render json: trip.errors, status: :unprocessable_entity
    end
  end

  private

  def trips_params
    params.permit(:start_address, :destination_address,
                  :price, :date)
  end

  def invalid_address?(address)
    address.nil? || address.empty?
  end
end
