class Api::StatsController < ApplicationController
  def weekly
    @weekly_stats = Trips::Statistics.get_weekly_stats(Date.today)
    render template: '/api/stats/weekly.json.jbuilder', formats: [:json]
  end

  def monthly
    @monthly_array_stats = Trips::Statistics.get_monthly_stats(Date.today)
    render template: '/api/stats/monthly.json.jbuilder', formats: [:json]
  end
end
